import { Injectable } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Filter } from '../models/filter.enum';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public filter: BehaviorSubject<string> = new BehaviorSubject(Filter.all);

  constructor(private router: Router) {
    router.events
      .pipe(filter(event => event instanceof ActivationEnd))
      .pipe(map((event: ActivationEnd) => event.snapshot.data.filter))
      .subscribe( navFilter => this.filter.next(navFilter));
  }
}
