import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Filter } from '../models/filter.enum';
import { Todo } from '../models/todo.model';
import { NavigationService } from './navigation.service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  public todos: BehaviorSubject<Array<Todo>> = new BehaviorSubject([]);
  public filteredTodos: BehaviorSubject<Array<Todo>> = new BehaviorSubject([]);
  public leftCount: BehaviorSubject<number> = new BehaviorSubject(0);
  private lastId = 0;

  constructor(private navigationService: NavigationService) {
    this.navigationService.filter.subscribe((newFilter: Filter) => {
      this.filteredTodos.next(this.getFilteredTodos(newFilter, this.todos.value));
    });
    this.todos.subscribe(todos => {
      this.filteredTodos.next(this.getFilteredTodos(this.navigationService.filter.value, todos));
    });
  }

  public addTodo(todoText: string) {
    const newTodo = {id: ++this.lastId, text: todoText, completed: false, editing: false};
    this.todos.next([...this.todos.value, newTodo]);
    this.updateLeftCount();
  }

  public removeTodo(todoId: number) {
    this.todos.next(this.todos.value.filter(
      (todo: Todo) => todo.id !== todoId
    ));
    this.updateLeftCount();
  }

  public toggleCompleted(todoId: number) {
    this.todos.next(this.todos.value.map(
      (todo: Todo) => {
        if (todo.id === todoId) {
          todo.completed = !todo.completed;
        }
        return todo;
      }
    ));
    this.updateLeftCount();
  }

  public clearAllCompleted() {
    this.todos.next(this.todos.value.filter(
      (todo: Todo) => todo.completed === false
    ));
  }

  public toggleAll() {
    this.todos.next(this.todos.value.map(
      (todo: Todo) => ({...todo, completed: !!this.leftCount.value})
    ));
    this.updateLeftCount();
  }

  public setEdit(todoId: number) {
    this.todos.next(this.todos.value.map(
      (todo: Todo) => ({...todo, editing: todo.id === todoId})
    ));
  }

  public updateItem(todoId: number, todoText: string) {
    this.todos.next(this.todos.value.map(
      (todo: Todo) => ({
        ...todo,
        editing: false,
        text: (todo.id === todoId ? todoText : todo.text),
      })
    ));
  }

  private updateLeftCount() {
    this.leftCount.next(this.todos.value.filter(
      (todo: Todo) => !todo.completed
    ).length);
  }

  private getFilteredTodos(newFilter, todos) {
    return todos.filter((todo: Todo) => {
      switch (newFilter) {
        case Filter.all:
          return true;
        case Filter.completed:
          return todo.completed;
        case Filter.active:
          return !todo.completed;
      }
    });
  }
}
