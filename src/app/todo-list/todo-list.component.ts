import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Todo } from '../models/todo.model';
import { NavigationService } from '../services/navigation.service';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todos: BehaviorSubject<Array<Todo>>;
  leftCount: BehaviorSubject<number>;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todos = this.todoService.filteredTodos;
    this.leftCount = this.todoService.leftCount;
  }

  removeTodo(todoId: number) {
    this.todoService.removeTodo(todoId);
  }

  toggleCompleted(todoId: number) {
    this.todoService.toggleCompleted(todoId);
  }

  toggleAll() {
    this.todoService.toggleAll();
  }

  edit(todo: Todo) {
    this.todoService.setEdit(todo.id);
  }

  update(todoId: number, todoText: string) {
    this.todoService.updateItem(todoId, todoText);
  }
}
