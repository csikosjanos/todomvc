import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Filter } from './models/filter.enum';
import { TodoListComponent } from './todo-list/todo-list.component';

const routes: Routes = [
  { path: 'active', component: TodoListComponent, data: {filter: Filter.active}},
  { path: 'completed', component: TodoListComponent, data: {filter: Filter.completed}},
  { path: '', component: TodoListComponent, pathMatch: 'full', data: {filter: Filter.all}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
