import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-header',
  templateUrl: './todo-header.component.html',
  styleUrls: ['./todo-header.component.css']
})
export class TodoHeaderComponent implements OnInit {
  public newTodo = '';

  constructor(private todoService: TodoService) { }

  ngOnInit() {
  }

  public addTodo(todoText: string) {
    this.todoService.addTodo(todoText);
    this.newTodo = '';
  }
}
