import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Filter } from '../models/filter.enum';
import { Todo } from '../models/todo.model';
import { NavigationService } from '../services/navigation.service';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css']
})
export class TodoFooterComponent implements OnInit {
  todos: BehaviorSubject<Array<Todo>>;
  todoLeftCount: BehaviorSubject<number>;
  filter: BehaviorSubject<string>;

  constructor(private todoService: TodoService, private navigationService: NavigationService) { }

  ngOnInit() {
    this.todos = this.todoService.todos;
    this.todoLeftCount = this.todoService.leftCount;
    this.filter = this.navigationService.filter;
  }

  clearAllCompleted() {
    this.todoService.clearAllCompleted();
  }

  isAll() {
    return this.filter.value === Filter.all;
  }

  isActive() {
    return this.filter.value === Filter.active;
  }

  isCompleted() {
    return this.filter.value === Filter.completed;
  }
}
