export enum Filter {
  all = 'ALL',
  active = 'ACTIVE',
  completed = 'COMPLETED',
}
